//
//  Constants.swift
//  NLSGForms
//
//  Created by Jim Aven on 3/31/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

let ADD_USER_INFO_SEG = "addUserInfoSeg"
let SEG_PLAYER_ENROLL = "playerEnrollSeg"
let SEG_USER_PAGE = "userSegue"
let SEG_SIGNED_IN = "signedInSegue"
let SEG_ADD = "addVCSegue"

let SHADOW_COLOR:CGFloat = 157.0 / 255.0

var vPARTICIPANT:Participant!
var vGUARDIAN:Guardian!
var vHAS_ANOTHER = false


//Firebase

let URL_FIREBASE = "https://nlsg.firebaseio.com/"
var vCUR_UID:String!

//MailGun

let API_MAILGUN = "key-86ef007060730c214a4aaeb14b3490cc"

//SendGrid
let API_SENDGRID = "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"

//notifications
let NOT_ALERT_FIELD = "textFieldMissing"
let NOT_CHANGE_VIEW = "changeView"
let NOT_BACK_VIEW = "backView"
let NOT_SUBMIT_FORM = "submitForm"
let NOT_DISMISS_VC = "dissmissVC"
let NOT_USER_SIGNED_UP = "userSignedUp"
let NOT_INVALID_EMAIL = "invalidEmail"
let NOT_CANCEL_VIEW = "cancelView"
let NOT_DISMISS_VIEW = "dismissView"


// Protocols

protocol SubmitParticpantDelegate {
    func submitFormConfirmAlert()
}

protocol NewMemberSignin {
    func nextVC(view:Int)
    func errorWithAlert(title:String, message:String, logout:Bool)
    func dismissView()
    func showSpinner(message:String)
    func hideSpinner()
}

protocol ChangeViewsDelegate {
    func showSpinner(message:String)
    func hideSpinner()
    func nextView(viewTag:Int)
    func emailAlert(title:String, message:String)
    func backView()
    func nextViewAlert()
    func nextVC()
}


