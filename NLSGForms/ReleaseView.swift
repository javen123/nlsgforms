//
//  ReleaseView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/11/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

class ReleaseView:UIView {
    
    let nibName = "ReleaseView"
    
    var changeDelegate:ChangeViewsDelegate!
    var submitDelegate:SubmitParticpantDelegate!
    
    @IBOutlet var view: UIView!
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        self.addSubview(loadViewFromNib())
        self.view.tag = 4
        layer.cornerRadius = 10
    }
    
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
        
        
    }

    
    @IBAction func btnBackPressed(sender: UIButton) {
        
        self.changeDelegate.backView()
        
    }
    @IBAction func btnSubmitPressed(sender: CustomButton) {
        
        self.submitDelegate.submitFormConfirmAlert()
        
    }
}