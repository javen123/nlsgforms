//
//  SendEmail.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/17/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation
import SendGrid

class SendEmail {
    
    let sg = SendGrid(apiKey: API_SENDGRID)
    
    
    var error:NSError?
    
    init(name:String, file:NSData) {
        let email = SendGrid.Email()
        do {
            try email.addTo("jim@appsneva.com", name: "Registration") // info@nlsportsgroup.com
            email.setFrom("no-reply@nlsportsgroup.com", name: "Info")
            email.setSubject("Registration: \(name)")
            email.setTextBody("File uploaded to database")
            email.addAttachment("\(name).csv", data: file)

        } catch {
            print("error with SendGrid")
        }
        do {
            try sg.send(email) {
                response, data, error in
                
                if error != nil {
                    print("error: \(error?.localizedDescription)")
                } else {
                    print(response)
                    print(data)
                }
            }
        } catch {
            print(error)
        }
    }
    
    deinit{
        
    }
}
