//
//  Guardian2.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/11/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

class GuardianView2:UIView, UITextFieldDelegate {
    
    @IBOutlet var guardianFields: [UITextField]!
    @IBOutlet weak var lblGuardian: UILabel!
    @IBOutlet var view: UIView!
    @IBOutlet weak var btnNext: CustomButton!
    
    var guardian = Guardian()
    var address = Address()
    var guardianDelegate:ChangeViewsDelegate!
    
    let nibName = "GuardianView2"
    
   
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        self.addSubview(loadViewFromNib())
        layer.cornerRadius = 10
        for textField in self.guardianFields {
            textField.delegate = self
            textField.backgroundColor = UIColor.whiteColor()
            textField.returnKeyType = .Next
            if textField.tag == 1 {
                textField.becomeFirstResponder()
            }
            
        }
        self.btnNext.userInteractionEnabled = false
        self.btnNext.alpha = 0.6
    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
        
        
    }

    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 4 || textField.tag == 5 || textField.tag == 6 {
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            
            return false
            
        } else {
            return true
        }

    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.enableBtnNext()
        return true
    }
    
    func textFieldShouldEndEditing(textField: UITextField) -> Bool {
        if textField.tag == 7 {
            if self.isValidEmail(textField.text!) {
               
            } else {
                self.guardianDelegate.emailAlert("Oops", message: "Need a valid email")
            }
         
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        textField.resignFirstResponder()
        if nextTag < 12 {
            let nextField = self.viewWithTag(nextTag) as! UITextField
            nextField.becomeFirstResponder()
        }
        
        return false
    }

    
    @IBAction func btnSamePressed(sender: UIButton) {
        
        for textField in self.guardianFields {
            if textField.tag == 8 {
                textField.text = vGUARDIAN.address.street
                
            } else if textField.tag == 9 {
                textField.text = vGUARDIAN.address.city
                
            }else if textField.tag == 10 {
                textField.text = vGUARDIAN.address.state
            }else if textField.tag == 11 {
                textField.text = String(vGUARDIAN.address.zip)
            }
        }
    }
    
    @IBAction func btnSkipPressed(sender:UIButton) {
        self.guardianDelegate.nextVC()
        
    }
    
    @IBAction func btnBackPressed(sender: UIButton) {
        
        self.guardianDelegate.backView()
    }
    
    @IBAction func btnNextPressed(sender: UIButton) {
        
        for textField in self.guardianFields {
            if textField.tag == 1 {
                if let _firstName = textField.text where !textField.text!.isEmpty {
                    self.guardian.firstName = _firstName
                }
            } else if textField.tag == 2 {
                if let _lastName = textField.text where !textField.text!.isEmpty {
                    self.guardian.lastName = _lastName
                }
            } else if textField.tag == 3 {
                if let _relation = textField.text where !textField.text!.isEmpty {
                    self.guardian.relation = _relation
                }
            } else if textField.tag == 4 {
                if let _cell = textField.text where !textField.text!.isEmpty {
                    self.guardian.cell = Double(self.getNumFromString(_cell))
                }
            } else if textField.tag == 5 {
                if let _home = textField.text where !textField.text!.isEmpty {
                    self.guardian.home = Double(self.getNumFromString(_home))
                }
            } else if textField.tag == 6 {
                if let _work = textField.text where !textField.text!.isEmpty {
                    self.guardian.work = Double(self.getNumFromString(_work))
                }
            }  else if textField.tag == 7 {
                if let _email = textField.text where !textField.text!.isEmpty {
                    self.guardian.email = _email
                }
            } else if textField.tag == 8 {
                if let _street = textField.text where !textField.text!.isEmpty  {
                    self.address.street = _street
                }
            } else if textField.tag == 9 {
                if let _city = textField.text where !textField.text!.isEmpty  {
                    self.address.city = _city
                }
            } else if textField.tag == 10 {
                if let _state = textField.text where !textField.text!.isEmpty  {
                    self.address.state = _state
                }
            } else if textField.tag == 11 {
                if let _zip = textField.text where !textField.text!.isEmpty {
                    self.address.zip = Double(_zip)
                    var _data = [String:AnyObject]()
                    _data["firstName"] = self.guardian.firstName
                    _data["lastName"] = self.guardian.lastName
                    _data["age"] = String(self.guardian.age)
                    
                    var _address = [String:AnyObject]()
                    
                    _address["street"] = self.address.street
                    _address["city"] = self.address.city
                    _address["state"] = self.address.state
                    _address["zip"] = String(self.address.zip)
                    
                    _data["address"] = _address
                    
                    self.guardianDelegate.showSpinner("Computing")
                    FirebaseHelper.ds.updateGuardian(["guardian2" : _data], completed: {
                        success in
                        if success {
                            self.guardianDelegate.hideSpinner()
                            self.guardianDelegate.nextVC()
                        } else {
                            self.guardianDelegate.hideSpinner()
                            self.guardianDelegate.emailAlert("Error", message: "Something went wrong with the internets")
                        }
                    })
                }
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

    func getNumFromString(textNum:String) -> String {
        let stringArray = textNum.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let _formNum = stringArray.joinWithSeparator("")
        return _formNum
    }
    
    func enableBtnNext () {
        for x in 1...4 {
            if let _field = self.viewWithTag(x) as? UITextField {
                if !_field.text!.isEmpty {
                    for y in 7...11 {
                        if let _aField = self.viewWithTag(y) as? UITextField {
                            if !_aField.text!.isEmpty {
                                self.btnNext.userInteractionEnabled = true
                                self.btnNext.alpha = 1.0
                            } else {
                                self.btnNext.userInteractionEnabled = false
                                self.btnNext.alpha = 0.6
                            }
                        }
                    }
                }
            }
        }
    }
}
