//
//  Participant.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/5/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

struct Address {
    var street:String!
    var city:String!
    var state:String!
    var zip:Double!
    init(street:String, city:String, state:String, zip:Double) {
        self.street = street
        self.city = city
        self.state = state
        self.zip = zip
    }
    
    init(){
        
    }
}

struct EmergencyContact {
    var firstName:String!
    var lastName:String!
    var relation:String!
    var phone:Double!
    var email:String!
    
    init () {
        
    }
    init (emContact:[String:AnyObject]) {
        firstName = emContact["firstName"] as! String
        lastName = emContact["lastName"] as! String
        relation = emContact["relation"] as! String
        phone = emContact["phone"] as! Double
        email = emContact["email"] as! String
    }
}


class Participant {
    
    private var _firstName:String!
    private var _lastName:String!
    private var _address:Address!
    private var _gender:Bool!
    private var _dob:NSDate!
    private var _age:Int!
    private var _grade:Int!
    private var _school:String!
    private var _phone:Double!
    private var _height:Double!
    private var _weight:Double!
    private var _notes:String!
    private var _guardian:[Guardian]!
    private var _league:League!
    private var _emergencyContact:EmergencyContact!
    private var _uid:String!
    
    var uid:String! {
        get {return _uid}
        set {_uid = newValue}
    }
    
    var emergencyContact:EmergencyContact! {
        get {return _emergencyContact}
        set {_emergencyContact = newValue}
    }
    
    var notes:String! {
        get {return _notes}
        set {_notes = newValue}
    }
    
    var league:League! {
        get {return _league}
        set {_league = newValue}
    }
    
    var address:Address {
        get {return _address}
        set {_address = newValue}
    }
    
    var firstName:String! {
        set {_firstName = newValue}
        get {return _firstName}
    }
    
    var lastName:String! {
        set {_lastName = newValue}
        get {return _lastName}
    }
    
    var gender:Bool! {
        set {_gender = newValue}
        get {return _gender}
    }
    
    var dob:NSDate! {
        set {_dob = newValue}
        get {return _dob}
    }
    
    var age:Int! {
        set {_age = newValue}
        get {return _age}
    }
    
    var grade:Int! {
        get {return _grade}
        set {_grade = newValue}
    }
    
    var school:String! {
        get {return _school}
        set {_school = newValue}
    }
    
    var phone:Double {
        set {_phone = newValue}
        get {return _phone}
    }
    
    var height:Double! {
        set {_height = newValue}
        get {return _height}
    }
    
    var weight:Double! {
        set {_weight = newValue}
        get {return _weight}
    }
    
    var guardian:[Guardian] {
        set {_guardian = newValue}
        get {return _guardian}
    }
    
    init () {
        
    }
    
    init (data:[String:AnyObject]) {
        
        self.firstName = data["firstName"] as! String
        self.lastName = data["lastName"] as! String
        self.age = data["age"] as! Int
        
        
        
        if let _isMale = data["isMale"] as? Bool {
            self.gender = _isMale
        }
        
        if let _wt = data["weight"] as? NSNumber{
            self.weight = Double(_wt)
        }
        if let _ht = data["height"] as? NSNumber {
            self.height = Double(_ht)
        }
        if let _g = data["grade"] as? NSNumber {
            self.grade = Int(_g)
        }
        if let _s = data["school"] as? String {
            self.school = _s
        }
        if let _em = data["emergencyContact"] as? [String:AnyObject] {
            self.emergencyContact = EmergencyContact(emContact: _em)
        }
        if let _d = data["dob"] as? Double {
            let aDate = NSDate(timeIntervalSince1970: _d)
            self.dob = aDate
        }
        if let _add = data["address"] as? [String:AnyObject] {
            if let _street = _add["street"] as? String {
                if let _city = _add["city"] as? String {
                    if let _st = _add["state"] as? String {
                        if let _z = _add["zip"] as? Double {
                            self.address = Address(street: _street, city: _city, state: _st, zip: _z)
                        }
                    }
                }
            }
        }
        
    }
    
    
}
