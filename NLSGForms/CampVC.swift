//
//  Form1VC.swift
//  NLSGForms
//
//  Created by Jim Aven on 3/31/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import Formatter
import InputValidator
import Validation
import FormTextField
import pop
import Spinner


enum EnrollType {
    case Player
    case Adult
}

class CampVC: UIViewController, UITextFieldDelegate, ChangeViewsDelegate, SubmitParticpantDelegate {
    
    var enType = EnrollType.Player
    var spinner = Spinner()
    
    @IBOutlet weak var imgGroup: UIImageView!
    
    let notification = NSNotificationCenter.defaultCenter()
    let imgBull = UIImage(named: "bull_NLSG")
    let imgCamp = UIImage(named: "sports_camps_NLSG")
    let imgSpeed = UIImage(named: "speed_performance_NLSG")
    let imgAdult = UIImage(named: "adult_game_NLSG")
    
    var isSameGuardian = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.formSelectorOnStart()
        if vPARTICIPANT == nil {
            vPARTICIPANT = Participant()
        }
        
    }
    
    func formSelectorOnStart() {
        
        if self.enType == .Player {
            
            let _view = Player()
            _view.center = AnimationEngine.offScreenRightPosition
            _view.changeDelegate = self
            self.view.addSubview(_view)
            AnimationEngine.animateToPosition(_view, position: AnimationEngine.screenCenterPosition, completed: {
                anim, success in
                
            })
        } else {
            let _view = EmergencyView()
            _view.center = AnimationEngine.offScreenRightPosition
            _view.changeDelegate = self
            self.view.addSubview(_view)
            AnimationEngine.animateToPosition(_view, position: AnimationEngine.screenCenterPosition, completed: {
                anim, success in
                
            })
        }
    }
    
    
    // MARK: ChangeViewsDelegates
    
    func showSpinner(message: String) {
        
        self.spinner.showInView(self.view, withTitle: message)
    }
    
    func hideSpinner() {
        self.spinner.hide()
    }
    
    func nextVC() {
        
        
    }
    
    func nextView(view:Int) {
        var _view:UIView!
        
        switch self.enType {
        case .Player:
            if view == 1 {
                let _aView = EmergencyView()
                _aView.changeDelegate = self
                _view = _aView
                
            } else if view == 2 {
                let _aView = NotesView()
                _aView.changeDelegate = self
                _view = _aView
            } else if view == 3 {
                let _aView = ReleaseView()
                _aView.changeDelegate = self
                _aView.submitDelegate = self
                _view = _aView
            }
        default:
            if view == 2 {
                let _aView = ReleaseView()
                _aView.changeDelegate = self
                _aView.submitDelegate = self
                _view = _aView
                
            }
        }
        
        AnimationEngine.animateToPosition(self.view.subviews.last!, position: AnimationEngine.offScreenLeftPosition) {
            anim, success in
            
            _view.center = AnimationEngine.offScreenRightPosition
            self.view.addSubview(_view)
            
            AnimationEngine.animateToPosition(_view, position: AnimationEngine.screenCenterPosition, completed: {
                anim, success in
                
            })
        }
    }
    
    func backView() {
        
        let _curView = self.view.subviews.last!
        AnimationEngine.animateToPosition(_curView, position: AnimationEngine.offScreenRightPosition) {
            anim, succes in
            _curView.removeFromSuperview()
            let _preView = self.view.subviews.last!
            
            if succes {
                AnimationEngine.animateToPosition(_preView, position: AnimationEngine.screenCenterPosition, completed: {
                    anim, succes in
                    
                })
            }
        }
    }
    
    func nextViewAlert() {
        
    }
    
    func emailAlert(title: String, message: String) {
        
    }
    
    //MARK: SubmitForm Delegate
    
    func submitFormConfirmAlert() {
        
        let alert = UIAlertController(title: "Confirm", message: "By clicking Yes you agree to the terms and conditions", preferredStyle: UIAlertControllerStyle.Alert)
        let yesAction = UIAlertAction(title: "Yes", style: .Default, handler: {
            action in
            
            print(self.navigationController?.viewControllers.count)
            
            self.spinner.showInView(self.view, withTitle: "Doing neat stuff in the cloud")
            
            var _part = [String:AnyObject]()
            
            if self.enType == .Player {
                
                let _address = vPARTICIPANT.address
                
                _part["guardian"] = vGUARDIAN.uid
                _part["firstName"] = vPARTICIPANT.firstName
                _part["lastName"] = vPARTICIPANT.lastName
                _part["address"] = ["street":_address.street, "city":_address.city, "state":_address.state, "zip":_address.zip]
                _part["isMale"] = vPARTICIPANT.gender
                _part["dob"] = NSNumber(double: vPARTICIPANT.dob.timeIntervalSince1970)
                _part["age"] = vPARTICIPANT.age
                _part["height"] = vPARTICIPANT.height
                _part["weight"] = vPARTICIPANT.weight
                _part["dateSigned"] = NSNumber(double: NSDate().timeIntervalSince1970)
                _part["notes"] = vPARTICIPANT.notes
                
                let _grade = NSNumber(integer: vPARTICIPANT.grade)
                let _school = vPARTICIPANT.school
                _part["grade"] = _grade
                _part["school"] = _school
                _part["emergencyContact"] = ["firstName":vPARTICIPANT.emergencyContact.firstName, "lastName":vPARTICIPANT.emergencyContact.lastName, "phone":vPARTICIPANT.emergencyContact.phone]
            } else {
                
                let _address = vGUARDIAN.address
                
                _part["guardian"] = vGUARDIAN.uid
                _part["firstName"] = vGUARDIAN.firstName
                _part["lastName"] = vGUARDIAN.lastName
                _part["cell"] = NSNumber(double:vGUARDIAN.cell)
                
                if let _home = vGUARDIAN.home {
                    _part["home"] = NSNumber(double:_home)
                }
                if let _work = vGUARDIAN.work {
                    _part["work"] = NSNumber(double:_work)
                }
                _part["address"] = ["street":_address.street, "city":_address.city, "state":_address.state, "zip":_address.zip]
                _part["isMale"] = vGUARDIAN.relation
                _part["age"] = Double(vGUARDIAN.age)
                _part["dateSigned"] = NSNumber(double: NSDate().timeIntervalSince1970)
            }
            
            FirebaseHelper.ds.createParticipant(_part, completed: {
                success in
                if success {
                    let _uid = vPARTICIPANT.uid
                    let _participant = ["participants":[_uid:true]]
                    FirebaseHelper.ds.updateGuardian(_participant, completed: { (success) in
                        if success {
                            self.spinner.hide()
                            self.performSegueWithIdentifier(SEG_USER_PAGE, sender: self)
                            
                            if self.enType == .Player {
                                CSVConverter.convertPlayer(vPARTICIPANT)
                            } else {
                                CSVConverter.convertPlayer(vGUARDIAN)
                            }
                            vGUARDIAN = nil
                            vPARTICIPANT = nil
                        }
                    })
                } else {
                    self.spinner.hide()
                }
            })
        })
        
        let noAction = UIAlertAction(title: "No", style: .Destructive, handler: nil)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnBackPressed(sender: CustomButton) {
        
        
    }
}

