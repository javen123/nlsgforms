//
//  UsersVC.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/24/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class UsersVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var firstName: UILabel!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var street: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var state: UILabel!
    @IBOutlet weak var zip: UILabel!
    @IBOutlet weak var cell: UILabel!
    @IBOutlet weak var home: UILabel!
    @IBOutlet weak var work: UILabel!
    @IBOutlet weak var guardianTableView: UITableView!
    @IBOutlet weak var participantTableView: UITableView!
    @IBOutlet weak var guardiansView: MaterialView!
    @IBOutlet weak var participantsView: MaterialView!
    
    
    var user:Guardian!
    var guardians = [Guardian]()
    var participants = [Participant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.guardianTableView.delegate = self
        self.guardianTableView.dataSource = self
        self.guardianTableView.tag = 2
        
        self.participantTableView.delegate = self
        self.participantTableView.dataSource = self
        self.participantTableView.tag = 1
        
    }
    
    override func viewWillAppear(animated: Bool) {
        
        self.participants = []
        self.guardians = []
        
        self.fetchUserInfo {
            success in
            if success {
                self.setupLabels()
                for x in self.user.partString {
                    print(self.user.partString)
                    self.fetchParticipants(x, complete: {
                        success in
                        if success {
                            print("parts success")
                            
                        } else {
                            print("no parts success")
                        }
                    })
                }
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let _vc = segue.destinationViewController as? AddPlayerGuardianVC {
            if segue.identifier == SEG_ADD {
                if sender!.tag == 1 {
                    let _index = self.participantTableView.indexPathForSelectedRow?.row
                    let _part = self.participants[_index!]
                    _vc.participant = _part
                } else {
                    let _index = self.guardianTableView.indexPathForSelectedRow?.row
                    let _guard = self.guardians[_index!]
                    _vc.guardian = _guard
                }
            }
        } else {
            print("can't segue for some reason")
        }
    }
    
    //MARK: TableView Delegates
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        var count:Int!
        
        if tableView == self.guardianTableView {
            count = 1
        }
        
        if tableView == self.participantTableView {
            count = 1
        }
        
        return count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var count:Int!
        
        if tableView == self.guardianTableView {
            count = self.guardians.count
        }
        
        if tableView == self.participantTableView {
            count = self.participants.count
        }
        return count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if tableView == self.participantTableView {
            let _cell = tableView.dequeueReusableCellWithIdentifier("partCell") as! ParticipantsCell
            let _part = self.participants[indexPath.row]
            _cell.configureCells(_part)
            return _cell
        } else {
            let _cell = tableView.dequeueReusableCellWithIdentifier("guardianCell") as! GuardianCell
            let _guard = self.guardians[indexPath.row]
            _cell.configureCells(_guard)
            return _cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier(SEG_ADD, sender: tableView)
    }
    
    func hideUnhideTables() {
        
        if self.guardians.isEmpty {
            self.guardiansView.hidden = true
            print("Is empty")
        } else {
            self.guardiansView.hidden = false
            print("Is not empty")
        }
        if self.participants.isEmpty {
            self.participantsView.hidden = true
            print("P is empty")
        } else {
            self.participantsView.hidden = false
            print("P is not empty")
        }
    }
    
    
    
    //MARK: Buttons
    @IBAction func btnBackPressed(sender: CustomButton) {
        do {
            try FIRAuth.auth()?.signOut()
        } catch {
            print("sign out error")
        }
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }
    
    @IBAction func btnLogoutPressed(sender: CustomButton) {
        
    }
    
    @IBAction func btnAddGuardian(sender: CustomButton) {
        
    }
    
    @IBAction func btnAddParticipant(sender: CustomButton) {
        
    }
    
    @IBAction func switchReceiveEmails(sender: UISwitch) {
        
    }
    
    @IBAction func switchReceiveTexts(sender: UISwitch) {
    }
    
    //MARK:FirebaseDatabase
    
    func fetchUserInfo(complete:(Bool)->Void) {
        
        if let _user = FIRAuth.auth()?.currentUser {
            let _uid = _user.uid
            
            FirebaseHelper.ds.ref.child("guardians").child(_uid).observeEventType(.Value, withBlock: {
                snap in
                let _data = snap.value as! [String:AnyObject]
                self.user = Guardian(uid: _uid, dict: _data)
                complete(true)
                
            })
        } else {
            print("there is no info")
            complete(false)
            
        }
    }
    
    func fetchParticipants(part:String,complete:(Bool) -> Void) {
        
        FirebaseHelper.ds.ref.child("participants").child(part).observeEventType(.Value, withBlock: {
            snap in
            if let _data = snap.value as? [String:AnyObject] {
                let _part = Participant(data: _data)
                self.participants.append(_part)
                self.participantTableView.reloadData()
                self.guardianTableView.reloadData()
                self.hideUnhideTables()
                complete(true)
            } else {
                print("No parts to download")
                complete(false)
            }
        })
    }
    
    func setupLabels() {
        
        self.firstName.text = self.user.firstName
        self.lastName.text = self.user.lastName
        self.street.text = self.user.address.street
        self.city.text = self.user.address.city
        self.state.text = self.user.address.state
        self.zip.text = String(self.user.address.zip)
        
        if let _cell = self.user.cell {
            var _fCell = String(_cell).stringByReplacingOccurrencesOfString(".0", withString: "")
            print(_fCell)
            _fCell.insert("(", atIndex: _fCell.startIndex)
            _fCell.insert(")", atIndex: _fCell.startIndex.advancedBy(4))
            _fCell.insert("-", atIndex: _fCell.startIndex.advancedBy(8))
            self.cell.text = _fCell
        }
        
        if let _home = self.user.home {
            var _fCell = String(_home).stringByReplacingOccurrencesOfString(".0", withString: "")
            _fCell.insert("(", atIndex: _fCell.startIndex)
            _fCell.insert(")", atIndex: _fCell.startIndex.advancedBy(4))
            _fCell.insert("-", atIndex: _fCell.startIndex.advancedBy(8))
            self.home.text = _fCell
        }
        
        if let _work = self.user.work {
            var _fCell = String(_work).stringByReplacingOccurrencesOfString(".0", withString: "")
            _fCell.insert("(", atIndex: _fCell.startIndex)
            _fCell.insert(")", atIndex: _fCell.startIndex.advancedBy(4))
            _fCell.insert("-", atIndex: _fCell.startIndex.advancedBy(8))
            self.work.text = _fCell
        }
    }
    
}
