//
//  GuardianView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/10/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

class GuardianView:UIView, UITextFieldDelegate {
    
    @IBOutlet var guardianFields: [UITextField]!
    @IBOutlet weak var lblGuardian: UILabel!
    @IBOutlet weak var stackAddress: UIStackView!
    @IBOutlet weak var stackName: UIStackView!
    @IBOutlet weak var btnNext: CustomButton!
    @IBOutlet var view: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var address = Address()
    var guardianDelegate:ChangeViewsDelegate!
    
    let nibName = "GuardianView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            self.setup()
        }
    }
    
    func setup() {
        
        self.addSubview(loadViewFromNib())
        
        for textField in self.guardianFields {
            textField.delegate = self
            textField.backgroundColor = UIColor.whiteColor()
            textField.returnKeyType = .Next
            if textField.tag == 1 {
                textField.becomeFirstResponder()
            }
        }
        
        self.btnNext.userInteractionEnabled = false
        self.btnNext.alpha = 0.6
        self.datePicker.setValue(UIColor.blackColor(), forKey: "textColor")
        self.datePicker.setValue(UIColor.whiteColor(), forKey: "backgroundColor")
        
        if vGUARDIAN == nil {
            vGUARDIAN = Guardian()
        }
        
    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let replacementText = string.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        if textField.tag == 3 || textField.tag == 4 || textField.tag == 5 {
            
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            
            return false
            
        } else if textField.tag == 9 {
            
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 5 {
                return false
            }
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigitCharacterSet()
            for char in replacementText.unicodeScalars
            {
                if !digits.longCharacterIsMember(char.value)
                {
                    return false
                }
            }
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.enableBtnNext()
        return true
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        textField.resignFirstResponder()
        if nextTag < 12 {
            let nextField = self.viewWithTag(nextTag) as! UITextField
            nextField.becomeFirstResponder()
        }
        
        return true
    }
    
    @IBAction func btnNextPressed(sender: UIButton) {
        
        for textField in self.guardianFields {
            if textField.tag == 1 {
                if let _firstName = textField.text where !textField.text!.isEmpty {
                    vGUARDIAN.firstName = _firstName
                }
            } else if textField.tag == 2 {
                if let _lastName = textField.text where !textField.text!.isEmpty {
                    vGUARDIAN.lastName = _lastName
                }
            } else if textField.tag == 3 {
                if let _cell = textField.text where !textField.text!.isEmpty {
                    
                    vGUARDIAN.cell = Double(self.getNumFromString(_cell))
                }
            } else if textField.tag == 4 {
                if let _home = textField.text where !textField.text!.isEmpty {
                    vGUARDIAN.home = Double(self.getNumFromString(_home))
                }
            } else if textField.tag == 5 {
                if let _work = textField.text where !textField.text!.isEmpty {
                    vGUARDIAN.work = Double(self.getNumFromString(_work))
                }
            } else if textField.tag == 6 {
                if let _street = textField.text where !textField.text!.isEmpty  {
                    self.address.street = _street
                }
                
            } else if textField.tag == 7 {
                if let _city = textField.text where !textField.text!.isEmpty  {
                    self.address.city = _city
                }
                
            } else if textField.tag == 8 {
                if let _state = textField.text where !textField.text!.isEmpty  {
                    self.address.state = _state
                }
            } else if textField.tag == 9  {
                if let _zip = textField.text where !textField.text!.isEmpty {
                    
                    self.address.zip = Double(_zip)
                    vGUARDIAN.address = self.address
                    let _age = self.setCurrentAge(self.datePicker.date)
                    
                    if _age > 17 {
                        vGUARDIAN.age = _age
                        var _data = [String:AnyObject]()
                        _data["firstName"] = vGUARDIAN.firstName
                        _data["lastName"] = vGUARDIAN.lastName
                        _data["age"] = String(vGUARDIAN.age)
                        _data["cell"] = NSNumber(double: vGUARDIAN.cell)
                        if let _home = vGUARDIAN.home {
                            _data["home"] = NSNumber(double: _home)
                        }
                        if let _work = vGUARDIAN.work {
                            _data["work"] = NSNumber(double:_work)
                        }
                        var _address = [String:AnyObject]()
                        _address["street"] = vGUARDIAN.address.street
                        _address["city"] = vGUARDIAN.address.city
                        _address["state"] = vGUARDIAN.address.state
                        _address["zip"] = String(vGUARDIAN.address.zip)
                        
                        _data["address"] = _address
                        
                        self.guardianDelegate.showSpinner("Computing")
                        FirebaseHelper.ds.updateGuardian(_data, completed: {
                            success in
                            if success {
                                self.guardianDelegate.hideSpinner()
                                self.guardianDelegate.nextViewAlert()
                            } else {
                                self.guardianDelegate.hideSpinner()
                                self.guardianDelegate.emailAlert("Error", message: "Something went wrong with the internets")
                            }
                        })
                    }
                }
            } else if textField.tag == 12 {
                if let _howHeard = textField.text {
                    vGUARDIAN.howHeard = _howHeard
                }
            }
        }
    }
    
    func enableBtnNext () {
        for x in 1...4 {
            if let _field = self.viewWithTag(x) as? UITextField {
                if !_field.text!.isEmpty {
                    for y in 7...11 {
                        if let _aField = self.viewWithTag(y) as? UITextField {
                            if !_aField.text!.isEmpty {
                                self.btnNext.userInteractionEnabled = true
                                self.btnNext.alpha = 1.0
                            } else {
                                self.btnNext.userInteractionEnabled = false
                                self.btnNext.alpha = 0.6
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getNumFromString(textNum:String) -> String {
        let stringArray = textNum.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let _formNum = stringArray.joinWithSeparator("")
        return _formNum
    }
    
    func setCurrentAge(date:NSDate) -> Int {
        
        let _date = NSCalendar.currentCalendar()
        let _age = _date.components(.Year, fromDate: date, toDate: NSDate(), options: [])
        print(_age.year)
        return _age.year
        
    }
}
