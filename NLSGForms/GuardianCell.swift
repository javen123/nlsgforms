//
//  GuardianCell.swift
//  NLSGForms
//
//  Created by Jim Aven on 6/1/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit

class GuardianCell: UITableViewCell {

    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    @IBOutlet weak var lblRelation: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    func configureCells(guardian:Guardian) {
        self.lblFirstName.text = guardian.firstName
        self.lblLastName.text = guardian.lastName
        self.lblRelation.text = guardian.relation
    }

}
