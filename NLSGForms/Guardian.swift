//
//  Guardian.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/5/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation


class Guardian {
    
    private var _firstName:String!
    private var _lastName:String!
    private var _relationToPlayer:String!
    private var _email:String!
    private var _cell:Double!
    private var _home:Double?
    private var _work:Double?
    private var _address:Address!
    private var _participants:[Participant]!
    private var _age:Int!
    private var _uid:String!
    private var _addGuardian:[Guardian]!
    private var _howHeard:String!
    
    var howHeard:String {
        get {return _howHeard}
        set {_howHeard = newValue}
    }
    
    var addGuardians:[Guardian] {
        get {return _addGuardian}
        set {_addGuardian = newValue}
    }
    
    var uid:String {
        get {return _uid}
        set {_uid = newValue}
    }
    
    var participants:[Participant] {
        get {return _participants}
        set {_participants = newValue}
    }
    
    var age:Int! {
        get {return _age}
        set {_age = newValue}
    }
    
    
    var firstName:String! {
        get {return _firstName}
        set {_firstName = newValue}
    }
    
    var lastName:String! {
        get {return _lastName}
        set {_lastName = newValue}
    }
    
    var relation:String! {
        get {return _relationToPlayer}
        set {_relationToPlayer = newValue}
    }
    
    var email:String! {
        get {return _email}
        set {_email = newValue}
    }
    
    var cell:Double! {
        get {return _cell}
        set {_cell = newValue}
    }
    
    var home:Double? {
        get {return _home}
        set {_home = newValue}
    }
    
    var work:Double? {
        get {return _work}
        set {_work = newValue}
    }
    
    var address:Address! {
        get { return _address}
        set {_address = newValue}
    }
    
    var partString:[String]!
    
    init(){
        
    }
    
    init(uid:String, dict:[String:AnyObject]) {
        
        var _temp = [String]()
        
        self.uid = uid
        if let x = dict["firstName"] {
            self.firstName = x as! String
        }
        if let x = dict["lastName"] {
            self.lastName = x as! String
        }
        if let x = dict["cell"] {
            self.cell = x as! Double
        }
        if let x = dict["home"] {
            self.home = x as? Double
        }
        if let x = dict["work"] {
            self.work = x as? Double
        }
        
        if let x = dict["address"] as? [String:AnyObject] {
            self.address = Address(street: x["street"] as! String, city: x["city"] as! String, state: x["state"] as! String, zip: Double(x["zip"] as! String)!)
        }
        if let  _parts = dict["participants"] as? [String:AnyObject] {
           _temp = Array(_parts.keys)
            self.partString = _temp
            print(self.partString)
        
        } else {
            print("No parts")
        }
    }
}