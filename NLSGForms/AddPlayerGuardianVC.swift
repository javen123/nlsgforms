//
//  AddPlayerGuardianVC.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/26/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit

class AddPlayerGuardianVC: UIViewController {
    
    var guardian:Guardian!
    var participant:Participant!

    @IBOutlet var textFieldsCollection: [UITextField]!
    @IBOutlet weak var lblAge: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func setupView() {
        
        if self.guardian != nil {
            self.updateGuardianLabels()
        } else if self.participant != nil {
            
            self.updatePartLabels()
            
        }
    }
    @IBAction func btnBackPressed(sender: CustomButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func updatePartLabels() {
        
        for x in self.textFieldsCollection {
            switch x.tag {
            case 1:
                if let _fName = self.participant.firstName {
                    x.text = _fName
                    print(x.text)
                }
            case 2:
                if let _lName = self.participant.firstName {
                    x.text = _lName
                    print(x.text)
                }
            case 3:
                if let _grade = self.participant.grade {
                    x.text = String(_grade)
                    print(x.text)
                }
            case 4:
                if let _school = self.participant.school {
                    x.text = _school
                    print(x.text)
                }

            case 5:
                if let _ht = self.participant.height {
                    x.text = String(_ht)
                    print(x.text)
                
                }
            case 6:
                if let _wt = self.participant.weight {
                    x.text = String(_wt)
                    print(x.text)
                }
            case 7:
                if let _street = self.participant.address.street {
                    x.text = _street
                    print(x.text)
                }
            case 8:
                if let _city = self.participant.address.city {
                    x.text = _city
                    print(x.text)
                }
            case 9:
                if let _state = self.participant.address.state {
                    x.text = _state
                    print(x.text)
                }
            case 10:
                if let _zip = self.participant.address.zip {
                    x.text = String(_zip)
                    print(x.text)
                }
            case 11:
                if let _l = self.participant.league {
                    if let _le = _l.league {
                        x.text = _le
                    }
                }
            case 12:
                if let _l = self.participant.league {
                    if let _le = _l.division {
                        x.text = _le
                    }
                }
            case 13:
                if let _l = self.participant.league {
                    if let _le = _l.team {
                        x.text = _le
                    }
                }
            case 14:
                if let _l = self.participant.league {
                    if let _le = _l.coach {
                        x.text = _le
                    }
                }
            default:
                print("Switch statement sucked it!")
            }
        }
        
        if let _dob = self.participant.dob {
            self.datePicker.date = _dob
        self.lblAge.text = String(self.ageFromNSDate(self.participant.dob))
        }
    }
    
    func updateGuardianLabels() {
        
        for x in self.textFieldsCollection {
            switch x.tag {
            case 1:
                if let _fName = self.guardian.firstName {
                    x.text = _fName
                }
            case 2:
                if let _lName = self.guardian.firstName {
                    x.text = _lName
                }
            case 3:
                x.text = "Does not apply"
                x.userInteractionEnabled = false
            case 4:
                x.text = "Does not apply"
                x.userInteractionEnabled = false
            case 5:
                x.userInteractionEnabled = false
            case 6:
                x.userInteractionEnabled = false
            case 7:
                if let _street = self.guardian.address.street {
                    x.text = _street
                }
            case 8:
                if let _city = self.guardian.address.city {
                    x.text = _city
                }
            case 9:
                if let _state = self.guardian.address.state {
                    x.text = _state
                }
            case 10:
                if let _zip = self.guardian.address.zip {
                    x.text = String(_zip)
                }
            case 11:
                 x.userInteractionEnabled = false
            case 12:
                 x.userInteractionEnabled = false
            case 13:
                 x.userInteractionEnabled = false
            case 14:
                 x.userInteractionEnabled = false
            default:
                print("Switch statement sucked it!")
            }
        }
        if let _age = guardian.age {
            self.lblAge.text = String(_age)
        }
    }
    
    func ageFromNSDate(date:NSDate) -> Int {
        
        return NSCalendar.currentCalendar().components(.Year, fromDate: date, toDate: NSDate(), options: NSCalendarOptions(rawValue: 0)).year
        
    }
}
