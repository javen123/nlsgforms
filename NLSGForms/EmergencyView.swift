//
//  EmergencyView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/11/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation


class EmergencyView: UIView, UITextFieldDelegate {
    
    @IBOutlet weak var btnG1: UIButton!
    @IBOutlet var emergencyFields: [UITextField]!

    @IBOutlet var view: UIView!
    
    var emergencyContact = EmergencyContact()
    var changeDelegate:ChangeViewsDelegate!
    let nibName = "EmergencyView"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        
        self.addSubview(loadViewFromNib())
        self.view.tag = 2
        
        layer.cornerRadius = 10
        
        for textField in self.emergencyFields {
            textField.delegate = self
            textField.backgroundColor = UIColor.whiteColor()
            textField.returnKeyType = .Next
            if textField.tag == 1 {
                textField.becomeFirstResponder()
            }
        }

    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
        
        
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        if textField.tag == 4 {
            let newString = (textField.text! as NSString).stringByReplacingCharactersInRange(range, withString: string)
            let components = newString.componentsSeparatedByCharactersInSet(NSCharacterSet.decimalDigitCharacterSet().invertedSet)
            
            let decimalString = components.joinWithSeparator("") as NSString
            let length = decimalString.length
            let hasLeadingOne = length > 0 && decimalString.characterAtIndex(0) == (1 as unichar)
            
            if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
            {
                let newLength = (textField.text! as NSString).length + (string as NSString).length - range.length as Int
                
                return (newLength > 10) ? false : true
            }
            var index = 0 as Int
            let formattedString = NSMutableString()
            
            if hasLeadingOne
            {
                formattedString.appendString("1 ")
                index += 1
            }
            if (length - index) > 3
            {
                let areaCode = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("(%@)", areaCode)
                index += 3
            }
            if length - index > 3
            {
                let prefix = decimalString.substringWithRange(NSMakeRange(index, 3))
                formattedString.appendFormat("%@-", prefix)
                index += 3
            }
            
            let remainder = decimalString.substringFromIndex(index)
            formattedString.appendString(remainder)
            textField.text = formattedString as String
            
            return false
            
        } else {
            return true
        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        let nextTag = textField.tag + 1
        textField.resignFirstResponder()
        if nextTag < 6 {
            let nextField = self.viewWithTag(nextTag) as! UITextField
            nextField.becomeFirstResponder()
        }
        
        return false
    }
    @IBAction func btnSameAsGuardianPressed(sender: UIButton) {
        
        if sender.tag == 22 {
            self.sameAddressHelper(0)
        } else {
            self.sameAddressHelper(1)
        }
    }

    @IBAction func btnBackPressed(sender: UIButton) {
        
        self.changeDelegate.backView()
    }
    @IBAction func btnNextPressed(sender: UIButton) {
        
        for textField in self.emergencyFields {
            
            if !textField.text!.isEmpty {
                if textField.tag == 1 {
                    
                if let _firstName = textField.text where !textField.text!.isEmpty {
                        self.emergencyContact.firstName = _firstName
                    }
                } else if textField.tag == 2 {
                    if let _lastName = textField.text where !textField.text!.isEmpty {
                        self.emergencyContact.lastName = _lastName
                    }
                }else if textField.tag == 3 {
                    if let _relation = textField.text where !textField.text!.isEmpty {
                        self.emergencyContact.relation = _relation
                    }
                }else if textField.tag == 4 {
                    if let _cell = textField.text where !textField.text!.isEmpty  {
                        self.emergencyContact.phone = Double(self.getNumFromString(_cell))
                    }
                } else if textField.tag == 5 {
                    if let _email = textField.text where !textField.text!.isEmpty  {
                        if self.isValidEmail(_email) {
                            self.emergencyContact.email = _email
                            vPARTICIPANT.emergencyContact = self.emergencyContact
                            let _part = [vPARTICIPANT!]
                            vGUARDIAN.participants = _part
                            self.changeDelegate.nextView(self.view.tag)
 
                        } else {
                            self.changeDelegate.emailAlert("Oops", message: "Need a valid email")
                        }
                    }
                }
            } else {
                self.changeDelegate.emailAlert("Oops", message: "Fields must be filled")
            }
        }
    }
    
    func sameAddressHelper(tag:Int) {
        
        for textField in self.emergencyFields {
            
            if textField.tag == 1 {
                if let _firstName = vGUARDIAN.firstName {
                    textField.text = _firstName
                }
            } else if textField.tag == 2 {
                if let _lastName = vGUARDIAN.lastName{
                    textField.text = _lastName
                }
            }else if textField.tag == 3 {
                if let _relation = vGUARDIAN.relation {
                    textField.text = _relation
                }
            }else if textField.tag == 4 {
                if let _cell = vGUARDIAN.cell{
                    textField.text = String(_cell)
                }
            } else if textField.tag == 5 {
                if let _email = vGUARDIAN.email {
                    textField.text = _email
                }
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }

    func getNumFromString(textNum:String) -> String {
        let stringArray = textNum.componentsSeparatedByCharactersInSet(
            NSCharacterSet.decimalDigitCharacterSet().invertedSet)
        let _formNum = stringArray.joinWithSeparator("")
        return _formNum
    }
}