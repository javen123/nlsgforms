//
//  SigninView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/25/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import FirebaseAuth

class SigninView: UIView, UITextFieldDelegate {

    @IBOutlet weak var unField: UITextField!
    @IBOutlet weak var pwfield: UITextField!
    @IBOutlet var view: UIView!
    
    let nibName = "SigninView"
    
    var signinDelegate:NewMemberSignin!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        layer.cornerRadius = 10
        self.addSubview(loadViewFromNib())
        self.unField.becomeFirstResponder()
        self.unField.delegate = self
        self.pwfield.delegate = self
        self.view.tag = 2
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.unField {
            if self.isValidEmail(textField.text!) {
                textField.resignFirstResponder()
                self.pwfield.becomeFirstResponder()
                return true
            } else {
                textField.layer.borderColor = UIColor.redColor().CGColor
                return false
            }
        } else {
            if !textField.text!.isEmpty {
                return true
            } else {
                return false
            }
        }
        
    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
    }
    
    @IBAction func btnForgotPressed(sender: UIButton) {
        if let _email = self.unField.text {
            if self.isValidEmail(_email) {
                self.signinDelegate.showSpinner("Sending email to \(_email)")
                FIRAuth.auth()?.sendPasswordResetWithEmail(_email, completion: {
                    error in
                    if error != nil {
                        self.signinDelegate.hideSpinner()
                        
                    } else {
                        self.signinDelegate.hideSpinner()
                        self.signinDelegate.errorWithAlert("Success", message: "Your password reset has been sent", logout: true)
                    }
                    
                })
            } else {
                self.signinDelegate.errorWithAlert("Oops", message: "please input valid email", logout: false)
            }
        }
    }
    
    @IBAction func btnCancelPressed(sender: AnyObject) {
        self.signinDelegate.dismissView()
    }
    
    @IBAction func btnSubmitPressed(sender: CustomButton) {
        
        
        if let _email = self.unField.text {
            if let _pw = self.pwfield.text {
                self.signinDelegate.showSpinner("Signing in")
                FIRAuth.auth()?.signInWithEmail(_email, password: _pw, completion: {
                    user, error in
                    
                    if error != nil {
                        self.signinDelegate.hideSpinner()
                        self.signinDelegate.errorWithAlert("Oops", message: (error?.localizedDescription)!, logout: false)
                    } else {
                        self.signinDelegate.hideSpinner()
                        self.signinDelegate.nextVC(self.view.tag)
                    }
                })
                
                
            } else {
                self.signinDelegate.errorWithAlert("Missing Field", message: "Fill all fields", logout: false)
            }
        } else {
            self.signinDelegate.errorWithAlert("Missing Field", message: "Fill all fields", logout: false)
        }
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
}
