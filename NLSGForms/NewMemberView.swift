//
//  NewMemberView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/13/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import FirebaseAuth


class NewMemberView: UIView, UITextFieldDelegate {
    
    @IBOutlet weak var unField: UITextField!
    @IBOutlet weak var pwfield: UITextField!
    @IBOutlet var view: UIView!
    
    let nibName = "NewMemberView"
    
    var alertDelegate:NewMemberSignin!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        layer.cornerRadius = 10
        self.addSubview(loadViewFromNib())
        self.unField.becomeFirstResponder()
        self.unField.delegate = self
        self.pwfield.delegate = self
        self.view.tag = 1
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if textField == self.unField {
            if self.isValidEmail(textField.text!) {
                textField.resignFirstResponder()
                self.pwfield.becomeFirstResponder()
                return true
            } else {
                textField.layer.borderColor = UIColor.redColor().CGColor
                return false
            }
        } else {
            if !textField.text!.isEmpty {
                return true
            } else {
                return false
            }
        }

    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
    }

    
    @IBAction func btnCancelPressed(sender: AnyObject) {
        self.alertDelegate.dismissView()
    }
    
    @IBAction func btnSubmitPressed(sender: CustomButton) {
        
        if let _email = self.unField.text {
            if let _pw = self.pwfield.text {
                if let _ = FIRAuth.auth()?.currentUser {
                    
                    self.alertDelegate.errorWithAlert("Logout", message: "Please use the Returning button", logout: true)
                } else {
                    
                    FIRAuth.auth()?.createUserWithEmail(_email, password: _pw, completion: {
                        user, error in
                        if error != nil {
                            print("Error signing into firebase: \(error?.localizedDescription)")
                            self.alertDelegate.errorWithAlert("Oops", message: (error?.localizedDescription)!, logout: false)
                        } else {
                            var _newGData = [String:AnyObject]()
                            _newGData["provider"] = user?.providerID
                            _newGData["uid"] = user?.uid
                            _newGData["email"] = user?.email
                            FirebaseHelper.ds.createFBGuardian((user?.uid)!, guardian: _newGData, completed: { (success) in
                                if success {
                                    vGUARDIAN = Guardian()
                                    vGUARDIAN.email =  _email
                                    vGUARDIAN.uid = (user?.uid)!
                                    self.alertDelegate.nextVC(self.view.tag)
                                }
                            })
                        }
                    })
                }
            } else {
                self.alertDelegate.errorWithAlert("Missing Field", message: "Fill all fields", logout: false)
            }
        } else {
            self.alertDelegate.errorWithAlert("Missing Field", message: "Fill all fields", logout: false)
        }
    }


    func isValidEmail(testStr:String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }



}




