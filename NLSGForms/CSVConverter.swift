//
//  CSVConverter.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/12/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation

class CSVConverter {
    
    class func convertPlayer(ob:Any) {
        
        let playerString = NSMutableString()
        
        if let player = ob as? Participant{
            
            playerString.appendString("FirstName, LastName, Height, Weight, Grade, School, AddStreet, AddCity, AddSt, AddZip, League, Division, Team, Coach, Heard How? \n")
            playerString.appendString("\(player.firstName), \(player.lastName), \(String(player.height)), \(String(player.weight)), \(String(player.grade)), \(player.school), \(player.address.street),\(player.address.city), \(player.address.state), \(String(player.address.zip)), \(player.league.league), \(player.league.division), \(player.league.team), \(player.league.coach) \n")
            
            let file = "\(player.lastName).csv"
            if let dir = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.AllDomainsMask, true).first {
                let path = NSURL(fileURLWithPath: dir).URLByAppendingPathComponent(file)
                
                do {
                    try playerString.writeToURL(path, atomically: false, encoding: NSUTF8StringEncoding)
                    let data = NSData(contentsOfURL: path)
                    SendEmail(name: player.lastName, file: data!)
                    
                }
                catch {
                    print("Error: can't encode file format")
                }
            }
        }
    }
}
