//
//  FirebaseHelper.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/13/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class FirebaseHelper {
    
    static let ds = FirebaseHelper()
    
    let ref = FIRDatabase.database().reference()
    
    func createFBGuardian(uid:String, guardian:Dictionary<String, AnyObject>, completed:(success:Bool) -> Void) {
        ref.child("guardians").child(uid).setValue(guardian) {
            
            error, firebase in
            if error != nil {
                
                completed(success: false)
                
            } else {
                
                completed(success: true)
            }
        }
    }
    
    func refCurUser(completed:(success:Bool) -> Void) {
        
    }
    
    func createParticipant(part:[String:AnyObject], completed:(success:Bool) -> Void) {
        ref.child("participants").childByAutoId().setValue(part) {
            error, data in
            
            if error != nil {
                print(error!.localizedDescription)
                completed(success: false)
            } else {
                print("Participant added and attached to user")
                vPARTICIPANT.uid = data.key
                completed(success: true)
            }
        }
    }
    
    func updateGuardian(values:[String:AnyObject], completed:(success:Bool) -> Void){
        if let _user = FIRAuth.auth()?.currentUser?.uid {
            ref.child("guardians").child(_user).updateChildValues(values) {
                
                error, result in
                if error != nil {
                    print("update error: \(error!.localizedDescription)")
                    completed(success: false)
                } else {
                    
                    completed(success: true)
                }
            }
        }
    }
}
