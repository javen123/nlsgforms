//
//  ParticipantsCell.swift
//  NLSGForms
//
//  Created by Jim Aven on 6/1/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit

class ParticipantsCell: UITableViewCell {
    
    
    @IBOutlet weak var imgGender: UIImageView!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblFirstName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCells(part:Participant) {
        
        self.lblFirstName.text = part.firstName
        self.lblLastName.text = part.lastName
        
        if let _gender = part.gender {
            
            switch _gender {
            case true:
                self.imgGender.image = UIImage(named: "maleSmall")
            default:
                self.imgGender.image = UIImage(named: "femaleSmall")
            }
        }
    }
    
    
    @IBAction func btnEditPressed(sender: UIButton) {
    }
}
