//
//  League.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/10/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation


class League {
    
    private var _league:String!
    private var _division:String!
    private var _coach:String!
    private var _team:String!
    
    var team:String! {
        get {return _team}
        set {_team = newValue}
    }
    
    var league:String! {
        get {return _league}
        set {_league = newValue}
    }
    
    var division:String! {
        get {return _division}
        set { _division = newValue}
    }
    
    var coach:String! {
        get {return _coach}
        set {_coach = newValue}
    }
    
    
}