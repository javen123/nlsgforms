//
//  SignInVC.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/13/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import FirebaseAuth
import Spinner


class SignInVC: UIViewController, NewMemberSignin {
    
    let spinner = Spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    func goToAddInfoVC () {
        
        self.view.subviews.last?.removeFromSuperview()
        self.performSegueWithIdentifier(ADD_USER_INFO_SEG, sender: self)
    }
    
    @IBAction func btnNewPlayerPressed(sender: CustomButton) {
        
        let _view = NewMemberView()
        _view.alertDelegate = self
        _view.center = AnimationEngine.offScreenRightPosition
        self.view.addSubview(_view)

        self.loadView(_view)
    }
    
    @IBAction func btnReturningPressed(sender: CustomButton) {
        
        let _view = SigninView()
        _view.signinDelegate = self
        _view.center = AnimationEngine.offScreenRightPosition
        self.view.addSubview(_view)
        self.loadView(_view)
        
    }
    
    func loadView(view:UIView) {
        
        AnimationEngine.animateToPosition(view, position: AnimationEngine.screenCenterPosition) {
            anim, success in
            
        }
    }
    
    func transitionOffScreen() {
        let view = self.view.subviews.last!
        AnimationEngine.animateToPosition(view, position: AnimationEngine.offScreenLeftPosition) {
            anim, success in
            if success {
                view.removeFromSuperview()
            }
        }
    }
    
    //NewMember delegates
    
    func errorWithAlert(title:String, message:String, logout:Bool){
        var _action:UIAlertAction!
        let _alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        switch logout {
        case true:
            _action = UIAlertAction(title: "Logout", style: .Default, handler: {
                action in
                do {
                    try FIRAuth.auth()?.signOut()
                } catch {
                    
                }
            })
        default:
            _action = UIAlertAction(title: title, style: .Destructive, handler: nil)
            self.view.subviews.last?.removeFromSuperview()
        }
        
        _alert.addAction(_action)
        self.presentViewController(_alert, animated: true, completion: nil)
    }
    
    func dismissView() {
        self.view.subviews.last?.removeFromSuperview()
    }
    
    func nextVC(view:Int) {
        if view == 1 {
            self.performSegueWithIdentifier(ADD_USER_INFO_SEG, sender: self)
        } else {
            self.performSegueWithIdentifier(SEG_SIGNED_IN, sender: self)
        }
        
    }
    
    func showSpinner(message: String) {
        self.spinner.showInView(self.view, withTitle: message)
    }
    
    func hideSpinner() {
        self.spinner.hide()
    }

}
