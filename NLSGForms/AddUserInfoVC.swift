//
//  AddUserInfoVC.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/20/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit
import pop
import FirebaseAuth
import Spinner

class AddUserInfoVC: UIViewController, ChangeViewsDelegate {
    
    var isEnrollingMinor = false
    let spinner = Spinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setup() {
        let _view = GuardianView()
        _view.guardianDelegate = self
        _view.center = AnimationEngine.offScreenRightPosition
        self.view.addSubview(_view)
        
        AnimationEngine.animateToPosition(_view, position: AnimationEngine.screenCenterPosition) {
            anim, success in
            
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == SEG_PLAYER_ENROLL {
            let vc = segue.destinationViewController as! CampVC
            if self.isEnrollingMinor {
                vc.enType = .Player
            } else {
                vc.enType = .Adult
            }
        }
    }
    @IBAction func btnBackPressed(sender: CustomButton) {
        
        self.dismissViewControllerAnimated(true, completion: {
            vGUARDIAN = nil
        })
    }
    
    // GuardianUpdateDelegates
    
    func backView() {
        AnimationEngine.animateToPosition(self.view.subviews.last!, position: AnimationEngine.offScreenRightPosition) {
            pop, success in
            
            if success {
                self.view.subviews.last!.removeFromSuperview()
                let _previousView = self.view.subviews.last!
                AnimationEngine.animateToPosition(_previousView, position: AnimationEngine.screenCenterPosition, completed: {
                    pop, success in
                    
                })
            }
        }
    }
    
    func nextVC() {
        self.performSegueWithIdentifier(SEG_PLAYER_ENROLL, sender: self)
    }
    
    func nextView(view:Int) {
       
        //mandatory delegate has no purpose here
    }
    
    func nextViewAlert() {
        
        let alert = UIAlertController(title: "Are you enrolling a minor?", message: "", preferredStyle: .Alert)
        let _yesAction = UIAlertAction(title: "Yes", style: .Default, handler: {
            action in
            
            AnimationEngine.animateToPosition(self.view.subviews.last!, position: AnimationEngine.offScreenLeftPosition, completed: { (anim:POPAnimation!, Bool) in
                
                self.isEnrollingMinor = true
                let _view = GuardianView2()
                _view.guardianDelegate = self
                _view.center = AnimationEngine.offScreenRightPosition
                self.view.addSubview(_view)
                
                AnimationEngine.animateToPosition(_view, position: AnimationEngine.screenCenterPosition, completed: { (anim:POPAnimation!, Bool) in
                    
                })
            })
        })
        
        let _noAction = UIAlertAction(title: "No", style: .Default, handler: {
            action in
            
            self.performSegueWithIdentifier(SEG_PLAYER_ENROLL, sender: self)
            
        })
        alert.addAction(_noAction)
        alert.addAction(_yesAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func emailAlert(title: String, message: String) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .Default, handler: nil)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showSpinner(message:String) {
        self.spinner.showInView(self.view, withTitle: message)
    }
    
    func hideSpinner() {
        self.spinner.hide()
    }
}
