//
//  FormView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/20/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit

@IBDesignable class FormView: UIView {

    override func awakeFromNib() {
        
        
        layer.backgroundColor = UIColor.blackColor().CGColor
        alpha = 0.85
        layer.cornerRadius = 10.0
        layer.shadowColor = UIColor(red: SHADOW_COLOR, green: SHADOW_COLOR, blue: SHADOW_COLOR, alpha: 0.5).CGColor
        self.frame = CGRectMake(center.x, center.y, 200, 200)
        layer.shadowOpacity = 0.8
        layer.shadowRadius = 5.0
        layer.shadowOffset = CGSizeMake(0.0, 2.0)
        
    }
}
