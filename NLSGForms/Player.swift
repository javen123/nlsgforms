//
//  Player.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/10/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import Foundation
import FormTextField

class Player: UIView, UITextFieldDelegate {
    
    var player = Participant()
    var league = League()
    var address = Address()
    
    @IBOutlet var view: UIView!
    @IBOutlet var playerFields: [FormTextField]!
    @IBOutlet weak var dob: UIDatePicker!
    @IBOutlet weak var btnMale: CustomButton!
    @IBOutlet weak var btnFemale: CustomButton!
    @IBOutlet weak var fieldGrade: FormTextField!
    @IBOutlet weak var fieldSchool: FormTextField!
    @IBOutlet weak var btnNext: CustomButton!
    
    let imgMale = UIImage(named: "btnMale")
    let imgMaleCheck = UIImage(named: "btnMaleCheck")
    let imgFemale = UIImage(named: "btnFemale")
    let imgFemaleCheck = UIImage(named: "btnFemaleCheck")
    let nibName = "Player"
    
    var changeDelegate:ChangeViewsDelegate!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            self.setup()
        }
    }
    
    func setup() {
        
        self.addSubview(loadViewFromNib())
        self.view.tag = 1
        
        if vPARTICIPANT == nil {
            vPARTICIPANT = Participant()
        }
        
        self.hasAnotherSetup(vHAS_ANOTHER)
        
        layer.cornerRadius = 10
        
        for textField in playerFields {
            textField.delegate = self
            textField.backgroundColor = UIColor.whiteColor()
            textField.returnKeyType = .Next
            
            if textField.tag == 1 {
                textField.becomeFirstResponder()
            }
        }
        
        self.btnNext.userInteractionEnabled = false
        self.btnNext.alpha = 0.6
        self.dob.setValue(UIColor.blackColor(), forKey: "textColor")
        self.dob.setValue(UIColor.whiteColor(), forKey: "backgroundColor")
        
    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
        
        
    }
    
    //MARK: TextField Delegates
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let replacementText = string.stringByReplacingOccurrencesOfString(" ", withString: "")
        
        if textField.tag == 3 {
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 1 {
                return false
            }
            
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigitCharacterSet()
            for char in replacementText.unicodeScalars
            {
                if !digits.longCharacterIsMember(char.value)
                {
                    return false
                }
            }
            return true
        } else if textField.tag == 5 {
            
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 3 {
                return false
            }
            
            if range.length == 1 {
                var mut = textField.text!
                mut.insert(".", atIndex: mut.endIndex)
                textField.text = mut
            }
            
            
        } else if textField.tag == 6 {
            
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 3 {
                return false
            }
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigitCharacterSet()
            for char in replacementText.unicodeScalars
            {
                if !digits.longCharacterIsMember(char.value)
                {
                    return false
                }
            }
        } else if textField.tag == 9 {
            
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 2 {
                return false
            }
            
        } else if textField.tag == 10 {
            
            if range.length > 0 {
                return true
            }
            if string == " " {
                return false
            }
            if range.location == 5 {
                return false
            }
            //Verify entered text is a numeric value
            let digits = NSCharacterSet.decimalDigitCharacterSet()
            for char in replacementText.unicodeScalars
            {
                if !digits.longCharacterIsMember(char.value)
                {
                    return false
                }
            }
        }
        return true
    }
    
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        self.enableBtnNext()
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.tag == 10 {
            self.enableBtnNext()
        }
        
        let nextTag = textField.tag + 1
        textField.resignFirstResponder()
        if nextTag < 15 {
            let nextField = self.viewWithTag(nextTag) as! FormTextField
            nextField.becomeFirstResponder()
        }
        
        return false
    }
    
    //MARK: Buttons
    
    @IBAction func btnNextPressed(sender: UIButton) {
        
        for textField in playerFields {
            
            if textField.tag == 1 {
                if let _firstName = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.firstName = _firstName
                }
            } else if textField.tag == 2 {
                if let _lastName = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.lastName = _lastName
                }
            } else if textField.tag == 3 {
                if let _grade = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.grade = Int(_grade)
                }
            } else if textField.tag == 4 {
                if let _school = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.school = _school
                }
            } else if textField.tag == 5 {
                if let _ht = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.height = Double(_ht)!
                }
            } else if textField.tag == 6 {
                if let _wt = textField.text where !textField.text!.isEmpty {
                    vPARTICIPANT.weight = Double(_wt)!
                }
            } else if textField.tag == 7 {
                if let _street = textField.text where !textField.text!.isEmpty  {
                    self.address.street = _street
                }
            } else if textField.tag == 8 {
                if let _city = textField.text where !textField.text!.isEmpty  {
                    self.address.city = _city
                }
            } else if textField.tag == 9 {
                if let _state = textField.text where !textField.text!.isEmpty  {
                    self.address.state = _state
                }
            } else if textField.tag == 10  {
                if let _zip = textField.text where !textField.text!.isEmpty {
                    self.address.zip = Double(_zip)
                }
            } else if textField.tag == 11 {
                if let _league = textField.text {
                    self.league.league = _league
                }
            }  else if textField.tag == 12 {
                if let _division = textField.text {
                    self.league.division = _division
                }
            }  else if textField.tag == 13 {
                if let _team = textField.text {
                    self.league.team = _team
                }
            }  else if textField.tag == 14 {
                if let _coach = textField.text {
                    self.league.coach = _coach
                    vPARTICIPANT.dob = self.dob.date
                    vPARTICIPANT.age = self.setCurrentAge(self.dob.date)
                    vPARTICIPANT.address = self.address
                    vPARTICIPANT.league = self.league
                    self.changeDelegate.nextView(self.view.tag)
                }
            }
        }
    }
    
    @IBAction func btnMalePressed(sender: UIButton) {
        
        if btnMale.imageView?.image == imgMale! {
            btnMale.setImage(self.imgMaleCheck, forState: .Normal)
            btnFemale.setImage(self.imgFemale, forState: .Normal)
            vPARTICIPANT.gender = true
        } else {
            btnMale.setImage(self.imgMale, forState: .Normal)
        }
    }
    @IBAction func btnFemalePressed(sender: UIButton) {
        
        if btnFemale.imageView?.image == imgFemale! {
            btnFemale.setImage(self.imgFemaleCheck, forState: .Normal)
            btnMale.setImage(self.imgMale, forState: .Normal)
            vPARTICIPANT.gender = false
        } else {
            btnFemale.setImage(self.imgFemale, forState: .Normal)
        }
        
        
    }
    
    @IBAction func btnSameAsGuardianPressed(sender: CustomButton) {
        
        for x in self.playerFields {
            if x.tag == 7 {
                x.text = vGUARDIAN.address.street
            } else if x.tag == 8 {
                x.text = vGUARDIAN.address.city
            } else if x.tag == 9 {
                x.text = vGUARDIAN.address.state
            } else if x.tag == 10 {
                x.text = String(vGUARDIAN.address.zip)
            }
        }
    }
    
    
    @IBAction func datePicker(sender: UIDatePicker) {
        vPARTICIPANT.age = setCurrentAge(sender.date)
    }
    
    func setCurrentAge(date:NSDate) -> Int {
        
        let _date = NSCalendar.currentCalendar()
        let _age = _date.components(.Year, fromDate: date, toDate: NSDate(), options: [])
        print(_age.year)
        return _age.year
        
    }
    
    func enableBtnNext() {
        for x in 1...10 {
            if let _field = self.viewWithTag(x) as? FormTextField {
                if !_field.text!.isEmpty {
                    self.btnNext.userInteractionEnabled = true
                    self.btnNext.alpha = 1.0
                } else {
                    self.btnNext.userInteractionEnabled = false
                    self.btnNext.alpha = 0.6
                    break
                }
            }
        }
    }
    
    func hasAnotherSetup(another:Bool) {
        if another {
            for x in self.playerFields {
                if x.tag == 7 {
                    x.text = vPARTICIPANT.address.street
                } else if x.tag == 8 {
                    x.text = vPARTICIPANT.address.city
                } else if x.tag == 9 {
                    x.text = vPARTICIPANT.address.state
                } else if x.tag == 10 {
                    x.text = String(vPARTICIPANT.address.zip)
                }
            }
        }
    }
}