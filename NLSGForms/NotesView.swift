//
//  NotesView.swift
//  NLSGForms
//
//  Created by Jim Aven on 5/17/16.
//  Copyright © 2016 JIm Aven. All rights reserved.
//

import UIKit

class NotesView: UIView {
    
    var changeDelegate:ChangeViewsDelegate!
    let nibName = "NotesView"
    
    @IBOutlet weak var txtViewNotes: UITextView!
    @IBOutlet var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        if self.subviews.count == 0 {
            setup()
        }
    }
    
    func setup() {
        self.addSubview(loadViewFromNib())
        layer.cornerRadius = 10
        self.view.tag = 3
    }
    
    func loadViewFromNib() -> UIView {
        
        let _bundle = NSBundle(forClass: self.dynamicType)
        let _nib = UINib(nibName: self.nibName, bundle: _bundle)
        self.view = _nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        self.bounds = self.view.bounds
        return self.view
        
        
    }

    
    @IBAction func btnBackPressed(sender: CustomButton) {
        self.changeDelegate.backView()
    }

    @IBAction func btnNextPressed(sender: CustomButton) {
        if let _notes = self.txtViewNotes.text {
            vPARTICIPANT.notes = _notes
            self.changeDelegate.nextView(self.view.tag)
        } else {
            self.changeDelegate.nextView(self.view.tag)
        }
    }
}
